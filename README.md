

# Custom Swing Components
This is a simple Custom Java Swing GUI component. It's call JSpinLoader. Its a loading
animation intended to be used to indicate that your program is loading or is busy
completing some task. 

To use simply include the JSpinLoader.java JArrow.java or JMediaButton file in your program, instantiate an
object instance and add to a JPanel object

A Demo programs are included showing the components features.

Compilation instructions are as follows... (browse to the src directory)

	$ javac JSpinLoaderDemo.java

that should compile all necessary .java files. to run the program type the command

	$ java JSpinLoaderDemo

Just do the same for other components like JArrow or JMediaButton

## Images
![Imgur](https://i.imgur.com/aufwe5K.gif)
screenshot of JSpinLoader

![Imgur](https://i.imgur.com/xyV3Uod.png)
screenshot of JArrow

![Imgur](https://i.imgur.com/w2wrbF9.png)
screenshot of JMediaButton
