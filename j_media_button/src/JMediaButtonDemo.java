
/**
 *
 * @author karma
 */
public class JMediaButtonDemo extends javax.swing.JFrame {

	/**
	 * Creates new form JMediaButtonDemo
	 */
	public JMediaButtonDemo() {
		initComponents();
	}

        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel3 = new javax.swing.JPanel();
                jPanel1 = new javax.swing.JPanel();
                jMediaButton1 = new JMediaButton();
                jMediaButton2 = new JMediaButton();
                jMediaButton3 = new JMediaButton();
                jMediaButton4 = new JMediaButton();
                jMediaButton5 = new JMediaButton();
                jMediaButton6 = new JMediaButton();
                jPanel2 = new javax.swing.JPanel();
                jMediaButton7 = new JMediaButton();
                jMediaButton8 = new JMediaButton();
                jMediaButton9 = new JMediaButton();
                jMediaButton10 = new JMediaButton();
                jMediaButton11 = new JMediaButton();
                jMediaButton12 = new JMediaButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                getContentPane().setLayout(new java.awt.FlowLayout());

                jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "JMediaButton Demo", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Noto Sans", 1, 16))); // NOI18N
                jPanel3.setLayout(new java.awt.GridLayout(2, 1));

                jMediaButton1.setText("jMediaButton1");
                jPanel1.add(jMediaButton1);

                jMediaButton2.setText("jMediaButton2");
                jMediaButton2.setmType(JMediaButton.MediaType.STOP);
                jPanel1.add(jMediaButton2);

                jMediaButton3.setText("jMediaButton3");
                jMediaButton3.setmType(JMediaButton.MediaType.PAUSE);
                jPanel1.add(jMediaButton3);

                jMediaButton4.setText("jMediaButton4");
                jMediaButton4.setmType(JMediaButton.MediaType.RECORD);
                jPanel1.add(jMediaButton4);

                jMediaButton5.setText("jMediaButton5");
                jMediaButton5.setmType(JMediaButton.MediaType.SKIP_BACK);
                jPanel1.add(jMediaButton5);

                jMediaButton6.setText("jMediaButton6");
                jMediaButton6.setmType(JMediaButton.MediaType.SKIP_FWD);
                jPanel1.add(jMediaButton6);

                jPanel3.add(jPanel1);

                jMediaButton7.setText("jMediaButton7");
                jMediaButton7.setColour(new java.awt.Color(0, 204, 51));
                jPanel2.add(jMediaButton7);

                jMediaButton8.setText("jMediaButton8");
                jMediaButton8.setColour(new java.awt.Color(0, 51, 204));
                jMediaButton8.setmType(JMediaButton.MediaType.STOP);
                jPanel2.add(jMediaButton8);

                jMediaButton9.setText("jMediaButton9");
                jMediaButton9.setColour(new java.awt.Color(0, 0, 0));
                jMediaButton9.setmType(JMediaButton.MediaType.PAUSE);
                jPanel2.add(jMediaButton9);

                jMediaButton10.setText("jMediaButton10");
                jMediaButton10.setColour(new java.awt.Color(255, 0, 0));
                jMediaButton10.setmType(JMediaButton.MediaType.RECORD);
                jPanel2.add(jMediaButton10);

                jMediaButton11.setText("jMediaButton11");
                jMediaButton11.setColour(new java.awt.Color(255, 102, 0));
                jMediaButton11.setmType(JMediaButton.MediaType.SKIP_BACK);
                jPanel2.add(jMediaButton11);

                jMediaButton12.setText("jMediaButton12");
                jMediaButton12.setColour(new java.awt.Color(255, 51, 255));
                jMediaButton12.setmType(JMediaButton.MediaType.SKIP_FWD);
                jPanel2.add(jMediaButton12);

                jPanel3.add(jPanel2);

                getContentPane().add(jPanel3);

                pack();
        }// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(JMediaButtonDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(JMediaButtonDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(JMediaButtonDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(JMediaButtonDemo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new JMediaButtonDemo().setVisible(true);
			}
		});
	}

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private JMediaButton jMediaButton1;
        private JMediaButton jMediaButton10;
        private JMediaButton jMediaButton11;
        private JMediaButton jMediaButton12;
        private JMediaButton jMediaButton2;
        private JMediaButton jMediaButton3;
        private JMediaButton jMediaButton4;
        private JMediaButton jMediaButton5;
        private JMediaButton jMediaButton6;
        private JMediaButton jMediaButton7;
        private JMediaButton jMediaButton8;
        private JMediaButton jMediaButton9;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JPanel jPanel2;
        private javax.swing.JPanel jPanel3;
        // End of variables declaration//GEN-END:variables
}
